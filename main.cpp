#include <iostream>
#include <string>
#include <iomanip>
#include "graph.h"
using namespace std;
int main()
{
      int n,x,y;
	  graph graph;
	  	
	  cout << "How many nodes: ";
	  cin >> n;
	  cout << endl;
	  // Dynamic allocation
	  
	  int** matrix = new int*[n];
	  for (int i = 0; i < n; ++i) {
		matrix[i] = new int[n];
	  }
	
	  // Input 
	  for (int i = 0; i < n; ++i) {
	    for (int j = 0; j < n; ++j) {
	      cout << "Input matrix[" << i << "][" << j << "]: ";
	      cin >> matrix[i][j];
	    }
	    cout << endl;
	  }
	  
	  cout << "--------------------------------" << endl;
	  // Output
	  cout << "\nAdjacent Matrix of this graph is: " << endl << endl;
	  for (int i = 0; i < n; ++i) {
	    for (int j = 0; j < n; ++j) {
	      cout << matrix[i][j] << " ";
	      if (j == n - 1) {
	        cout << endl;
	      }
		}
      }
      
	  cout << endl;
	  
	  graph.convert(matrix,n);
	  
	  graph.print();
	
	  cout << endl << "--------------------------------" << endl << endl;
	  cout << "Graph type table." << endl << endl;
	
	  cout << "MultiGraph               " << graph.multi_check() << endl;
	  cout << "PseudoGraph              " << graph.pseudo_check() << endl;
	  cout << "DirectGraph              " << graph.direct_check() << endl;
	  cout << "WeightGraph              " << graph.weight_check() << endl;
	  cout << "CompleteGraph            " << graph.complete_check() << endl;	  
  	  
}
